package com.formigas.spritradar.config

import com.formigas.spritradar.controller.BusinessLogicController
import com.formigas.spritradar.provider.ProviderFirebase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class ProviderConfiguration {
    private val provider = ProviderFirebase()
    private val controller = BusinessLogicController(provider)


    @Bean
    @Primary
    fun businessProvider(): BusinessLogicController = controller

}
