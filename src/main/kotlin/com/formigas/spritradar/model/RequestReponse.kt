package com.formigas.spritradar.model

data class AveragePricesPerStationRequest(
        val groupId: String,
        val stationId: String,
        val beginTimestamp: String,
        val endTimestamp: String,
)