package com.formigas.spritradar.model

data class PetrolStation(
    val id: String,
    val name: String,
    val brand: String,
    val street: String,
    val houseNr: String,
    val zip: String,
    val city: String,
    val position: LatLng,
    val openingHours: Map<String, String>,
    val currentPrice: PetrolPrice?,
)

data class LatLng(
    val lat: Float,
    val lon: Float,
)

data class PetrolPrice(
    val associatedStationId: String,
    val superE5DeciCents: Int?,
    val superE10DeciCents: Int?,
    val dieselDeciCents: Int?,
    val timestampIsoString: String?,
)

data class Stat(
    val id: String,
    val petrolPrice: Any?,
)
