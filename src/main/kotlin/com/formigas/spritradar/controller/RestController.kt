package com.formigas.spritradar.controller

import com.formigas.spritradar.model.AveragePricesPerStationRequest
import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.Stat
import com.formigas.spritradar.provider.ProviderFirebase
import com.google.api.core.ApiFuture
import com.google.cloud.firestore.WriteResult
import org.joda.time.DateTime
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import kotlin.concurrent.thread

@RestController
@CrossOrigin(origins = ["*"])
class RestController(private val controller: BusinessLogicController) {


    @GetMapping("/alive")
    fun addVehicle(): ResponseEntity<String> =
        ResponseEntity("I'm alive", HttpStatus.OK)


    @PostMapping("/specificStationPriceDevelopment")
    fun specificStationPriceDevelopment(@RequestBody request: AveragePricesPerStationRequest?): ResponseEntity<String> {
        try {
            DateTime.parse(request?.beginTimestamp)
            DateTime.parse(request?.endTimestamp)
            if (DateTime.parse(request?.beginTimestamp).isAfter(DateTime.parse(request?.endTimestamp))) {
                throw java.lang.IllegalArgumentException()
            }
            thread { controller.specificStationPriceDevelopment(request!!) }
        } catch (e: IllegalArgumentException) {
            return ResponseEntity("Invalid date Time", HttpStatus.BAD_REQUEST)
        }

        return ResponseEntity("", HttpStatus.OK)
    }

    @PostMapping("/generalPriceDevelopment")
    fun generalPriceDevelopment(@RequestBody request: AveragePricesPerStationRequest?): ResponseEntity<String> {
        try {
            DateTime.parse(request?.beginTimestamp)
            DateTime.parse(request?.endTimestamp)
            if (DateTime.parse(request?.beginTimestamp).isAfter(DateTime.parse(request?.endTimestamp))) {
                throw java.lang.IllegalArgumentException()
            }
            thread { controller.generalPriceDevelopment(request!!) }
        } catch (e: IllegalArgumentException) {
            return ResponseEntity("Invalid date Time", HttpStatus.BAD_REQUEST)
        }
        return ResponseEntity("", HttpStatus.OK)
    }

    @PostMapping("/specificStationCheapest")
    fun specificStationCheapest(@RequestBody request: AveragePricesPerStationRequest?): ResponseEntity<String> {
        try {
            DateTime.parse(request?.beginTimestamp)
            DateTime.parse(request?.endTimestamp)
            if (DateTime.parse(request?.beginTimestamp).isAfter(DateTime.parse(request?.endTimestamp))) {
                throw java.lang.IllegalArgumentException()
            }
            thread { controller.specificStationCheapest(request!!) }
        } catch (e: IllegalArgumentException) {
            return ResponseEntity("Invalid date Time", HttpStatus.BAD_REQUEST)
        }
        return ResponseEntity("", HttpStatus.OK)
    }

    @GetMapping("/groups/{groupID}/stats")
    fun getStats(
//        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String
    ): List<Stat> {
        return controller.getStats(groupID)
    }

    @DeleteMapping("/groups/{groupID}/stats/{statsID}")
    fun deleteStats(
//        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String,
        @PathVariable("statsID") statsID: String
    ) {
        val result = controller.deleteStat(groupID, statsID).get()
        print(result)
    }

}


class BusinessLogicController(private val firebase: ProviderFirebase) {
    fun specificStationPriceDevelopment(request: AveragePricesPerStationRequest) {
        val prices: Map<String, List<PetrolPrice>>? = firebase.fetchPrices(request.beginTimestamp, request.endTimestamp)
        val x: List<PetrolPrice>? =
            prices?.mapValues {
                it.value.find { price -> price.associatedStationId == request.stationId }
            }?.map { it.value }?.filterNotNull()
        if (x != null && x.isNotEmpty()) {
            firebase.saveStatistics(x, request.groupId)
        }
    }

    fun generalPriceDevelopment(request: AveragePricesPerStationRequest) {
        val prices: Map<String, List<PetrolPrice>>? = firebase
            .fetchPrices(request.beginTimestamp, request.endTimestamp)
        val x: List<PetrolPrice>? = prices?.mapValues { entries ->
            entries.value.filter {
                it.dieselDeciCents != null && it.superE5DeciCents != null && it.superE10DeciCents != null
            }.reduce { acc, value ->
                PetrolPrice(
                    "allStations",
                    value.dieselDeciCents!! + acc.dieselDeciCents!!,
                    value.superE5DeciCents!! + acc.superE5DeciCents!!,
                    value.superE10DeciCents!! + acc.superE10DeciCents!!,
                    timestampIsoString = entries.key
                )
            }.let {
                PetrolPrice(
                    associatedStationId = it.associatedStationId,
                    superE5DeciCents = it.superE5DeciCents!! / entries.value.size,
                    superE10DeciCents = it.superE10DeciCents!! / entries.value.size,
                    dieselDeciCents = it.dieselDeciCents!! / entries.value.size,
                    timestampIsoString = entries.key
                )
            }
        }?.entries?.map { it.value }

        if (x != null && x.isNotEmpty()) {
            firebase.saveStatistics(x, request.groupId)
        }
    }

    fun specificStationCheapest(request: AveragePricesPerStationRequest) {
        val prices: Map<String, List<PetrolPrice>>? = firebase.fetchPrices(request.beginTimestamp, request.endTimestamp)
        val x: List<PetrolPrice>? = prices?.mapValues {
            it.value.find { price -> price.associatedStationId == request.stationId }
        }?.mapKeys { DateTime(it.key) }
            ?.toList()
            ?.map { (Pair(it.first.hourOfDay().get().toString(), it.second)) }
            ?.groupBy { it.first }
            ?.mapValues { it.value.map { p -> p.second } }
            ?.mapValues { entries ->
                entries.value.filterNotNull()
                    .filter { a -> a.superE5DeciCents != null && a.superE10DeciCents != null && a.dieselDeciCents != null }
                    .reduce { acc, petrolPrice ->
                        PetrolPrice(
                            associatedStationId = "cheapestHours",
                            superE5DeciCents = acc.superE5DeciCents!! + petrolPrice.superE5DeciCents!!,
                            superE10DeciCents = acc.superE10DeciCents!! + petrolPrice.superE10DeciCents!!,
                            dieselDeciCents = acc.dieselDeciCents!! + petrolPrice.dieselDeciCents!!,
                            timestampIsoString = acc.timestampIsoString,
                        )
                    }.let {
                        PetrolPrice(
                            associatedStationId = it.associatedStationId,
                            superE5DeciCents = it.superE5DeciCents!! / entries.value.size,
                            superE10DeciCents = it.superE10DeciCents!! / entries.value.size,
                            dieselDeciCents = it.dieselDeciCents!! / entries.value.size,
                            timestampIsoString = it.timestampIsoString,
                        )
                    }
            }?.map { it.value }
        if (x != null && x.isNotEmpty()) {
            firebase.saveStatistics(x, request.groupId)
        }
    }

    fun getStats(groupID: String): List<Stat> {
        return firebase.getStatistics(groupID)
    }

    fun deleteStat(groupID: String, statsID: String): ApiFuture<WriteResult> {
        return firebase.deleteStat(groupID, statsID)
    }

}