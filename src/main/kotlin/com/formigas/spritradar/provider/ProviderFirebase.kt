package com.formigas.spritradar.provider

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.Stat
import com.google.api.core.ApiFuture
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import com.google.cloud.firestore.WriteResult
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.joda.time.DateTime

const val CLOUD_BUCKET_ENV_VAR = "CLOUD_BUCKET"
const val CLOUD_CREDENTIALS_ENV_VAR = "CLOUD_CREDENTIALS"

class ProviderFirebase {
    private val firestore: Firestore
    private val storage: Storage

    private val CLOUD_BUCKET: String = System.getenv(CLOUD_BUCKET_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_BUCKET")
    private val CREDENTIALS: String = System.getenv(CLOUD_CREDENTIALS_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_CREDENTIALS")


    init {
        ServiceAccountCredentials.fromStream(CREDENTIALS.byteInputStream(Charsets.UTF_8)).also { credentials ->
            firestore = FirestoreOptions.newBuilder().setCredentials(credentials).build().service
            storage = StorageOptions.newBuilder().setCredentials(credentials).build().service
        }
    }

    fun fetchPrices(startTimestamp: String, endTimestamp: String): Map<String, List<PetrolPrice>>? {
        val bucket = storage.list().iterateAll().find { bucket -> bucket.name == CLOUD_BUCKET }
        val pricesStrings = bucket?.list()?.iterateAll()?.distinctBy { it.name.subSequence(0, 13) }?.filter {
            DateTime.parse(it.name).isAfter(DateTime.parse(startTimestamp)) &&
                    DateTime.parse(it.name).isBefore(DateTime.parse(endTimestamp))
        }
        val petrolType = object : TypeToken<List<PetrolPrice>>() {}.type
        val prices: Map<String, List<PetrolPrice>>? = pricesStrings?.map {
            val pricesPer10Minutes = it.getContent().toString(Charsets.UTF_8)
            val x = Gson().fromJson<List<PetrolPrice>>(pricesPer10Minutes, petrolType)
            return@map Pair(it.name, x)
        }?.toMap()
        return prices
    }

    fun saveStatistics(stations: List<PetrolPrice>, groupId: String) {
        firestore.collection("groups")
            .document(groupId)
            .collection("statistics").add(mapOf(Pair("data", stations)))
                .get()
    }

    fun getStatistics(groupId: String): List<Stat> {
        return firestore.collection("groups").document(groupId).collection("statistics").listDocuments()
            .map { Stat(it.id, it.get().get().data?.get("data")) }
    }

    fun deleteStat(groupID: String, statsID: String): ApiFuture<WriteResult> {
        return firestore.collection("groups").document(groupID).collection("statistics").document(statsID).delete()
    }
}
